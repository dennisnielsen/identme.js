/**
 * Creates an new object, based on the original, with only the selected properties.
 * @param {Object} object
 * @param {Array} properties
 * @param {Boolean} [strict] throw error if property doesn't exists.
 * @returns {*}
 */
function select(object, properties, strict = false) {
    return properties.reduce((prev, prop) => {
        if(object[prop]) {
            prev[prop] = object[prop];
        } else {
            if(strict) {
                throw "Required property " + prop + " is not defined."
            }
        }

        return prev;
    }, {});
}

function merge(object, properties) {
    return {
        base: properties ? select(object, properties) : object,
        with: function(object, properties) {
            object = properties ? select(object, properties) : object;

            let merged = {};
            for(var attrname in this.base) { merged[attrname] = this.base[attrname]; }
            for(var attrname in object) { merged[attrname] = object[attrname]; }

            return merged;
        }
    };
}


export default {
    select: select,
    merge: merge
}