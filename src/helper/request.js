import http from 'superagent';
import version from '../version';

function Request(options) {
    this.baseOptions = options;
}

/**
 * Applies any provided headers to the request.
 * @param {Object} request superagent request object.
 * @param {Object} headers headers applied to the request object.
 * @returns {*}
 */
Request.prototype.applyHeaders = function(request, headers) {
    var keys = Object.keys(headers);
    for (var a = 0; a < keys.length; a++) {
        request = request.set(keys[a], headers[keys[a]]);
    }

    request.set('content-type', 'application/json; charset=utf-8');
    request.set('Identme-Client', JSON.stringify({name: 'identme.js', version: version.raw }));

    return request;
};

/**
 * @callback requestCallback
 * @param {Error} [error] reason for the failed request.
 * @param {Object} [result] response from the api server.
 */

/**
 * Sends a GET request to provided api url
 * @param {String} url relative url to the intended api call. eg. '/users'.
 * @param {Object} query querystring formatted as an object.
 * @param {requestCallback} callback
 */
Request.prototype.get = function(url, query, callback) {
    let req = http.get(this.baseOptions.rootUrl + url).query(query);
    req = this.applyHeaders(req, this.baseOptions.headers);

    req.end(callback);
};

/**
 * Sends a POST request to the provided url
 * @param {String} url relative url to the intended api call. eg. '/users'.
 * @param {Object} body the body being sent to the server.
 * @param {requestCallback} callback
 */
Request.prototype.post = function(url, body, callback) {
    let req = http.post(this.baseOptions.rootUrl + url).send(body);
    req = this.applyHeaders(req, this.baseOptions.headers);

    req.end(callback);
};


export default Request;