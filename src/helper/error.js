function response(error, description) {
    return {
        error: error,
        description: description
    }
}

export default {
    response: response
}