/**
 * Gets the window object
 * @returns {Window}
 */
function getWindow() {
    return window;
}

/**
 * Redirects the windows to a given url
 * @param url
 */
function redirect(url) {
    getWindow().location = url;
}


export default {
    redirect: redirect,
    getWindow: getWindow
}