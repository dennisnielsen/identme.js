import Auth from  '../auth';

import optionsHelper from '../helper/options';
import windowHelper from '../helper/window';
import qs from 'qs';
import error from "../helper/error";
import assert from 'assert-js';


/**
 * Handles all authentications flows.
 *
 * @param {Object} options
 * @param {String} options.clientID the client id generated in the identme-console.
 * @param {String} options.domain the domain name of your tenant.
 * @param {String} [options.redirectUri] the URL redirected to, when autentication is complete.
 * @param {String} [options.nonce] A cryptographically random nonce used to validate the authorization request, and is mandatory if you use OpenID in your scope.
 * @param {Array} [options.scope] Scope of the request, used as access levels in your application.
 * @param {String} [options.audience] Intended audience for the token, recieved by the authorization.
 * @constructor
 */
function Authenticator(options) {
    assert.object(options);
    assert.uuid(options.clientID);
    assert.string(options.domain);

    this.baseOptions = options;

    this.auth = new Auth(options);
}

/**
 * Begins an authorization against the resource server.
 * @param {Object} options
 * @param {String} options.clientID ID of your application client, set in the identme-console.
 * @param {String} options.domain Full domain name of your tenant.
 * @param {String} options.redirectUri The redirectUri for the client, set in the identme-console.
 * @param {String} [options.nonce] A cryptographically random nonce used to validate the authorization request, and is mandatory if you use OpenID in your scope.
 * @param {Array} [options.scope] Scope of the request, used as access levels in your application.
 */
Authenticator.prototype.authorize = function(options) {
    try {
        var params = optionsHelper
            .merge(this.baseOptions)
            .with(options);

        assert.object(params);
        assert.uuid(params.clientID);


        params = optionsHelper.select(
            params,
            [
                'clientID',
                'redirectUri',
                'responseType',
                'scope',
                'domain',
                'nonce',
                'audience'
            ]
        );

        windowHelper.redirect(
            this.auth.buildAuthorizationUri(params)
        );
    } catch (e) {
        console.log('Error', e);
    }
};

/**
 * Parses an incoming token response query.
 * @param {Object} options
 * @param {String} options.query
 * @param sc
 * @param ec
 * @returns {*}
 */
Authenticator.prototype.parseQuery = function(options, sc, ec) {
    options = options || {};
    ec = ec || function(error) {};

    const requestParts = ( options.query === undefined ? windowHelper.getWindow().location.href : options.query ).split('?');
    const queryString = requestParts.length < 2 ? null : requestParts[ requestParts.length - 1 ];
    const queryObjects = qs.parse(queryString);

    return this.validateTokenResponse(options, queryObjects, sc, ec);
};

Authenticator.prototype.validateTokenResponse = function(options, queryObjects, sc, ec) {

    if(!queryObjects.hasOwnProperty('access_token') || !queryObjects.hasOwnProperty('expires_in'))
        return ec(error.response('invalid_query', 'The access_token and expires_in parameters are both mandatory, but one or more is missing from the query.'));


    return sc(queryObjects);
};

export default Authenticator;