import urlJoin from 'url-join';
import env from '../env';
import Request from '../helper/request';

/**
 *
 * @param {Object} options
 * @param {String} options.domain your Identme domain.
 * @param {String} options.token a valid access token.
 * @constructor
 */
function ApiClient(options) {
    this.baseOptions = options;
    this.baseOptions.rootUrl = urlJoin(env.protocol + this.baseOptions.domain, 'api', 'v1');
    this.baseOptions.headers = {
        Authorization: 'Bearer ' + this.baseOptions.token,
        Accept: 'application/json'
    };

    this.request = new Request(this.baseOptions);
}

export { ApiClient }

export default ApiClient;