import env from '../env';
import assert from 'assert-js';

function Auth(options) {
    assert.object(options);
    assert.string(options.domain);

    this.baseOptions = options;
    this.baseOptions.rootUrl = env.protocol + this.baseOptions.domain;
};

/**
 * Generate authorization uri based on options.
 * @param {Object} options
 * @param {String} options.responseType
 * @param {String} options.clientID
 * @param {String} options.redirectUri
 * @param {Array} [options.scope]
 * @param {String} [options.nonce]
 * @param {String} [options.audience]
 * @param {String} [options.domain]
 * @returns {string}
 */
Auth.prototype.buildAuthorizationUri = function(options) {
    assert.object(options);
    assert.string(options.responseType);
    assert.uuid(options.clientID);
    assert.url(options.redirectUri);

    let authorizeUri = this.baseOptions.rootUrl + '/oauth/authorize?' +
        'response_type=' + options.responseType + '&' +
        'client_id=' + options.clientID + '&' +
        'redirect_uri=' + options.redirectUri + '&' +
        'scope=' + (options.scope || []).join(' ') + '&' +
        'audience=' + (options.audience || 'https://' + options.domain + '/userinfo') + '&' +
        'state=' + (options.nonce || '');

    return authorizeUri;
};

export default Auth;